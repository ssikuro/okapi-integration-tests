package net.sf.okapi.tkit.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import net.sf.okapi.common.FileUtil;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.XMLFileCompare;
import net.sf.okapi.common.integration.IntegrationtestUtils;
import net.sf.okapi.common.integration.RoundTripUtils;
import net.sf.okapi.filters.tmx.TmxFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripTmxTkitsIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private TmxFilter tmxFilter;
	
	@Rule
	public ErrorCollector errCol = new ErrorCollector();

	@Before
	public void setUp() throws Exception {
		tmxFilter = new TmxFilter();		
	}

	@After
	public void tearDown() throws Exception {
		tmxFilter.close();
	}

	@Test
	public void tmxFiles() throws FileNotFoundException, URISyntaxException {		
		// run top level files (without config)
		for (File file : IntegrationtestUtils.getTestFiles("/tmx/", Arrays.asList(".tmx"))) {
			runTest(false, file, "okf_tmx", null);
			runTest(true, file, "okf_tmx", null);
		}

		// run each subdirectory where we assume there is a custom config)
		for(File d : IntegrationtestUtils.getSubDirs("/tmx/"))
		{
			for(File c : IntegrationtestUtils.getConfigFile(d.getPath()))
			{
				for(File file : IntegrationtestUtils.getTestFiles(d.getPath(), Arrays.asList(".tmx"), true))
				{					
					String configName = Util.getFilename(c.getAbsolutePath(), false);
					String customConfigPath = c.getParent();
					runTest(false, file, configName, customConfigPath);
					runTest(true, file, configName, customConfigPath);
				}
			}
		}
	}

	// NOTE: "a_small_test2.tmx" and "ImportTest2C.tmx" failures are correct for Tkit Merge. Seems to be a bug
	// with the legacy merger and subflows
	private void runTest(boolean segment, File file, String configName, String customConfigPath)
			throws FileNotFoundException, URISyntaxException {
		String f = file.getName();
		LOGGER.info(f);
		String root = file.getParent() + File.separator;
		String xliff = root + f + ".xliff";
		String original = root + f;
		String tkitMerged = root + f + ".tkitMerged";
		String merged = root + f + ".merged";
		
		List<String> locales = FileUtil.guessLanguages(file.getAbsolutePath());		
		LocaleId source = LocaleId.fromString(locales.get(0));
		LocaleId target = LocaleId.EMPTY;
		if (locales.size() >= 2) {
			target = LocaleId.fromString(locales.get(1));
		}
		
		RoundTripUtils.extract(source, target, original, xliff, configName, customConfigPath, segment);		
		RoundTripUtils.merge(source, target, false, original, xliff, tkitMerged, configName, customConfigPath);
		XMLFileCompare compare = new XMLFileCompare();				
		try {
			assertTrue("Compare Lines: " + f, compare.compareFilesPerLines(tkitMerged, tkitMerged));
		} catch(Throwable e) {
			errCol.addError(e);
		}
	}
}
